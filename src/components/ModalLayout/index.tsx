import React, { useEffect, useRef } from 'react'
import ReactDOM from 'react-dom'
import { CloseIcon } from '../../assets/svg/CloseIcon'
import { useAppDispatch } from '../../hooks/useAppDispatch'
import { useAppSelector } from '../../hooks/useAppSelector'
import { hideModal } from '../../store/reducer'
import styles from './index.module.scss'

function ModalLayout({ children }: { children: React.ReactNode }) {
  const node = document.querySelector('#modal_root')
  const ref = useRef<HTMLDivElement>(null)
  const dispatch = useAppDispatch()
  const isModalOpen = useAppSelector((state) => state.app.isModalOpen)

  useEffect(() => {
    function handleClick(e: MouseEvent) {
      if (
        e.target instanceof Node &&
        !ref.current?.contains(e.target) &&
        node?.contains(e.target)
      ) {
        dispatch(hideModal())
      }
    }
    document.addEventListener('click', handleClick)

    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  if (!node) return null

  return ReactDOM.createPortal(
    isModalOpen && (
      <div className={styles.modal}>
        <div className={styles.modalInner}>
          <div className={styles.modalBox} ref={ref}>
            <button
              className={styles.modalClose}
              onClick={() => dispatch(hideModal())}
            >
              <CloseIcon />
            </button>
            {children}
          </div>
        </div>
      </div>
    ),
    node
  )
}

export default ModalLayout
