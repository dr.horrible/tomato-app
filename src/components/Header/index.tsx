import { Link } from 'react-router-dom'
import { ArrLeftIcon } from '../../assets/svg/ArrLeftIcon'
import { StatisticIcon } from '../../assets/svg/StatisticIcon'
import { TomatoIcon } from '../../assets/svg/TomatoIcon'
import Layout from '../Layout'
import styles from './index.module.scss'

interface IHeaderProps {
  href: string
}

function Header({ href }: IHeaderProps) {
  return (
    <div className={styles.header}>
      <Layout>
        <div className={styles.row}>
          <Link to='/' className={styles.logo}>
            <TomatoIcon />
            <span>pomodoro_box</span>
          </Link>
          <Link to={href}>
            {href === '/statistic' ? (
              <>
                <StatisticIcon />
                <span>Статистика</span>
              </>
            ) : (
              <>
                <ArrLeftIcon />
                <span>На главную</span>
              </>
            )}
          </Link>
        </div>
      </Layout>
    </div>
  )
}
export default Header
