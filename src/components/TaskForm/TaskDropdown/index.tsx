import { DeleteIcon } from '../../../assets/svg/DeleteIcon'
import { EditIcon } from '../../../assets/svg/EditIcon'
import { MinusIcon } from '../../../assets/svg/MinusIcon'
import { PlusRoundIcon } from '../../../assets/svg/PlusRoundIcon'
import styles from './index.module.scss'
import ModalDeleteTask from '../../ModalDeleteTask'
import { useAppDispatch } from '../../../hooks/useAppDispatch'
import { showModal } from '../../../store/reducer'

type func = () => void

interface ITaskDropdownProps {
  isMinVal: boolean
  addCount: func
  denyCount: func
  editTask: func
  deleteTask: func
}

function TaskDropdown({
  isMinVal,
  denyCount,
  addCount,
  editTask,
  deleteTask,
}: ITaskDropdownProps) {
  const dispatch = useAppDispatch()

  return (
    <div className={styles.dropdown}>
      <button className={styles.dropdownButton} onClick={addCount}>
        <PlusRoundIcon />
        <span>Увеличить</span>
      </button>
      <button
        className={styles.dropdownButton}
        disabled={isMinVal}
        onClick={denyCount}
      >
        <MinusIcon />
        <span>Уменьшить</span>
      </button>
      <button className={styles.dropdownButton} onClick={editTask}>
        <EditIcon />
        <span>Редактировать</span>
      </button>
      <button
        className={styles.dropdownButton}
        onClick={() => dispatch(showModal())}
      >
        <DeleteIcon />
        <span>Удалить</span>
      </button>
      <ModalDeleteTask handleClick={deleteTask} />
    </div>
  )
}

export default TaskDropdown
