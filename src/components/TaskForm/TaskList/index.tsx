import { useMemo } from 'react'
import { getTimeString } from '../../../helpers/getTime'
import { useAppSelector } from '../../../hooks/useAppSelector'
import { WORK_TIME } from '../../../constants'
import TaskItem from '../TaskItem'
import style from './index.module.scss'
import { ITasks } from '../../../store/reducer'

function TaskList({ list }: { list: Array<ITasks> }) {
  const stateTasks = useAppSelector((state) => state.app.tasks)

  const tomatoNum = useMemo(
    () =>
      stateTasks.reduce((sum, current) => {
        return sum + current.tomatos
      }, 0),
    [stateTasks]
  )

  return (
    <div className={style.taskList}>
      {list.length > 0 &&
        list.map((item, idx) => (
          <TaskItem
            key={list[idx].id}
            title={item.name}
            index={idx}
            tomatos={item.tomatos}
          />
        ))}
      <div className={style.time}>{getTimeString(tomatoNum * WORK_TIME)}</div>
    </div>
  )
}

export default TaskList
