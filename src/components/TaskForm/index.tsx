import { useRef, useEffect, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { useAppDispatch } from '../../hooks/useAppDispatch'
import { useAppSelector } from '../../hooks/useAppSelector'
import { addTask } from '../../store/reducer'
import Button from '../Button'
import styles from './index.module.scss'
import TaskList from './TaskList'

function TaskForm() {
  const tasksArr = useAppSelector((state) => state.app.tasks)
  const ref = useRef<HTMLInputElement>(null)
  const dispatch = useAppDispatch()
  const [IDs, setIDs] = useState<Array<string>>([])

  const addNewTask = () => {
    if (ref.current && ref.current.value !== '') {
      dispatch(
        addTask({
          name: ref.current.value,
          tomatos: 1,
          id: IDs[IDs.length - 1],
        })
      )
      ref.current.value = ''
    }
  }

  useEffect(() => {
    const newID = uuidv4()

    setIDs((prevState) => [...prevState, newID])
  }, [tasksArr])

  return (
    <div className={styles.taskForm}>
      <div className={styles.formInput}>
        <input type="text" placeholder="Название задачи" ref={ref} />
        <div className={styles.formButton}>
          <Button title="Добавить" color="green" onClick={() => addNewTask()} />
        </div>
        {tasksArr.length > 0 && <TaskList list={tasksArr} />}
      </div>
    </div>
  )
}

export default TaskForm
