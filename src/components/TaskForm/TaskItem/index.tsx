import { useEffect, useRef, useState } from 'react'
import { DropdownIcon } from '../../../assets/svg/DropdownIcon'
import { useAppDispatch } from '../../../hooks/useAppDispatch'
import {
  addTomatosTotal,
  denyTomatosTotal,
  editTaskTitle,
  hideModal,
  removeTask,
} from '../../../store/reducer'
import TaskDropdown from '../TaskDropdown'
import styles from './index.module.scss'

interface ITaskItemProps {
  title: string
  index: number
  tomatos: number
}

const root = document.querySelector('#root')

function TaskItem({ title, index, tomatos }: ITaskItemProps) {
  const [isEdit, setIsEdit] = useState(false)
  const [showDropdown, setShowDropdown] = useState(false)
  const ref = useRef<HTMLDivElement>(null)
  const titleRef = useRef<HTMLDivElement>(null)
  const dispatch = useAppDispatch()

  useEffect(() => {
    function handleClick(e: MouseEvent) {
      if (
        e.target instanceof Node &&
        !ref.current?.contains(e.target) &&
        root?.contains(ref.current)
      ) {
        setShowDropdown(false)
      }
    }
    document.addEventListener('click', handleClick)

    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  const addCount = () => {
    dispatch(addTomatosTotal(index))
  }

  const denyCount = () => {
    dispatch(denyTomatosTotal(index))
  }

  const editTask = () => {
    setIsEdit(true)
    setTimeout(() => {
      titleRef.current?.focus()
    })
  }

  const deleteTask = () => {
    dispatch(removeTask(index))
    dispatch(hideModal())
    setShowDropdown(false)
  }

  return (
    <div className={styles.task}>
      <div className={styles.taskRow}>
        <div className={styles.taskLeft}>
          <div className={styles.taskNum}>{tomatos}</div>
          <span
            className={styles.taskTitle}
            contentEditable={isEdit}
            suppressContentEditableWarning={isEdit}
            ref={titleRef}
            onBlur={(e) => {
              dispatch(
                editTaskTitle({
                  index,
                  prevTitle: title,
                  newTitle: e.target.innerText,
                })
              )
              setIsEdit(false)
            }}
          >
            {title}
          </span>
        </div>
        <div className={styles.taskRight} ref={ref}>
          <button
            className={styles.dropButton}
            onClick={() => setShowDropdown(true)}
          >
            <DropdownIcon />
          </button>
          <div className={styles.taskDropdown}>
            {showDropdown && (
              <TaskDropdown
                isMinVal={tomatos === 1}
                denyCount={denyCount}
                addCount={addCount}
                editTask={editTask}
                deleteTask={deleteTask}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default TaskItem
