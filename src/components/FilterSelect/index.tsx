import Select, { StylesConfig } from 'react-select'

const OPTIONS = [
  {
    value: 'currentWeek',
    label: 'Эта неделя',
  },
  {
    value: 'lastWeek',
    label: 'Прошедшая неделя',
  },
  {
    value: 'beforeLastWeek',
    label: '2 недели назад',
  },
]

const colorStyles: StylesConfig = {
  control: () => ({
    display: 'flex',
    alignItems: 'center',
    borderColor: 'transparent',
    backgroundColor: '#F4F4F4',
    height: '55px',
    padding: '0 15px',
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
  dropdownIndicator: (_, state) => ({
    transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : '',
    color: '#B7280F',
  }),
  option: (_, state) => ({
    height: '55px',
    display: 'flex',
    alignItems: 'center',
    padding: '0 15px',
    backgroundColor: state.isFocused ? '#C4C4C4' : '#F4F4F4',
    borderTop: '1px solid #DEDEDE',
  }),
  menu: (styles) => ({
    ...styles,
    borderRadius: '0',
    padding: '0',
    margin: '0',
    boxShadow: 'none',
  }),
  menuList: () => ({
    border: 'none',
    filter: 'drop-shadow(0px 10px 63px rgba(0, 0, 0, 0.07))',
  }),
}

export const FilterSelect = ({ onChangeSelect }: any) => (
  <Select
    options={OPTIONS}
    styles={colorStyles}
    defaultValue={OPTIONS[0]}
    isSearchable={false}
    onChange={(item: any) => onChangeSelect(item.value)}
  />
)
