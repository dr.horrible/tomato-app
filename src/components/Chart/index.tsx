import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import {
  BarChart,
  Bar,
  ResponsiveContainer,
  YAxis,
  CartesianGrid,
  Cell,
  XAxis,
} from 'recharts'
import { DAY_NAMES } from '../../constants'
import { getTimeString } from '../../helpers/getTime'

export interface IData {
  value?: string
  time?: number
}
interface IChart {
  data: Array<IData>
  onChange: Dispatch<SetStateAction<number>>
  active: number
}

const formatTickYAxis = (tick: any) =>
  tick === 0 ? '0' : getTimeString(tick / 60, true)
const formatTickXAxis = (_: any, i: number) => DAY_NAMES[i]

function Chart({ data, onChange, active }: IChart) {
  const [activeIndex, setActiveIndex] = useState(NaN)

  useEffect(() => {
    setActiveIndex(active)
  }, [active])

  const handleClick = (_: any, index: number) => {
    onChange(index)
  }

  return (
    <ResponsiveContainer width="100%" height="100%">
      <BarChart
        width={77}
        height={375}
        data={data}
        margin={{ bottom: 19, top: 50, right: 50 }}
      >
        <CartesianGrid vertical={false} />
        <rect fill="#ECECEC" height="50" width="100%" y="428" x="0" />
        <Bar dataKey="time" barSize={77} onClick={handleClick}>
          {data.map((_, index) => (
            <Cell
              cursor="pointer"
              fill={index === activeIndex ? '#DC3E22' : '#EA8A79'}
              key={`cell-${index}`}
            />
          ))}
        </Bar>
        <XAxis
          tickFormatter={formatTickXAxis}
          tick={{
            fill: '#DC3E22',
            cursor: 'pointer',
            fontWeight: 500,
          }}
          axisLine={false}
          tickLine={false}
          tickMargin={10}
          onClick={(tick: any) => onChange(tick.index)}
        />
        <YAxis
          tick={{ width: 100 }}
          orientation="right"
          axisLine={false}
          tickLine={false}
          tickFormatter={formatTickYAxis}
        />
      </BarChart>
    </ResponsiveContainer>
  )
}

export default Chart
