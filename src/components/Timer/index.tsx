import { useEffect, useState } from 'react'
import { PlusIcon } from '../../assets/svg/PlusIcon'
import { getTimeSec } from '../../helpers/getTime'
import { useAppDispatch } from '../../hooks/useAppDispatch'
import { useAppSelector } from '../../hooks/useAppSelector'
import {
  addTomatosDone,
  denyTomatosTotal,
  removeTask,
  setPauseTime,
  setStops,
  setWorkTime,
  setTomatoTime,
  setTotalTime,
  setFocus,
} from '../../store/reducer'
import { WORK_TIME, REST_TIME, REST_LONG_TIME } from '../../constants'
import Button from '../Button'
import styles from './index.module.scss'

let timerWorkId: ReturnType<typeof setTimeout>
let timerPauseId: ReturnType<typeof setTimeout>

const WORK_TIME_SEC = WORK_TIME * 60
const REST_TIME_SEC = REST_TIME * 60
const REST_LONG_TIME_SEC = REST_LONG_TIME * 60

function Timer() {
  const [sound] = useState(
    new Audio('https://noisefx.ru/noise_base/04/02036.mp3')
  )
  const tasks = useAppSelector((state) => state.app.tasks)
  const tomato = useAppSelector(
    (state) => state.app.statistic[state.app.statistic.length - 1].tomato
  )
  const dispatch = useAppDispatch()
  const [timerNum, setTimerNum] = useState(0)
  const [timerTomatoNum, setTimerTomatoNum] = useState(0)
  const [doneTasks, setDoneTasks] = useState(tomato)
  const [start, setStart] = useState(false)
  const [rest, setRest] = useState(false)
  const [pause, setPause] = useState(false)
  const [restCount, setRestCount] = useState(0)
  const [over, setOver] = useState(false)
  const [time, setTime] = useState(WORK_TIME_SEC)
  const [pauseSec, setPauseSec] = useState(0)

  useEffect(() => {
    if (time > 0 && start && !pause) {
      setOver(false)
      clearTimeout(timerPauseId)
      timerWorkId = setTimeout(() => {
        setTime(time - 1)
        dispatch(setWorkTime())
        dispatch(setTotalTime())
        setTimerTomatoNum((prevState) => prevState + 1)
      }, 1000)
    }

    if (pause) {
      timerPauseId = setTimeout(() => {
        setPauseSec((prevState) => prevState + 1)
        dispatch(setPauseTime())
        dispatch(setTotalTime())
      }, 1000)
    }

    if (time === 0) {
      if (!over) {
        setOver(true)
      }

      if (rest) {
        stopRestTimer()
        setRestCount((prevState) => prevState + 1)
      }
    }
  }, [time, start, pause, pauseSec])

  useEffect(() => {
    if (over && time === 0) {
      sound.play()
      setTimeout(() => {
        setRest(true)
        setTimerNum(timerNum + 1)
      }, 1000)
    }
  }, [over, time])

  useEffect(() => {
    if (rest) {
      timerNum % 4 === 0 ? setTime(REST_LONG_TIME_SEC) : setTime(REST_TIME_SEC)
    }
  }, [rest])

  const setTitleLeftBtn = () => {
    if (start) {
      if (!pause) {
        return 'Пауза'
      } else {
        return 'Продолжить'
      }
    } else {
      return 'Старт'
    }
  }

  const setTitleRightBtn = () => {
    if (pause) {
      if (rest) {
        return 'Пропустить'
      } else {
        return 'Сделано'
      }
    } else {
      return 'Стоп'
    }
  }

  const pauseTimer = () => {
    setPause(!pause)
    clearTimeout(timerWorkId)
  }

  const setNewTime = () => {
    clearTimeout(timerWorkId)
    setTime((prevState) => prevState + 60)
  }

  const stopTimer = () => {
    setTime(WORK_TIME_SEC)
    setOver(true)
    setStart(false)
    setPause(false)
    setRest(false)
    clearTimeout(timerWorkId)
    clearTimeout(timerPauseId)

    if (pause) {
      dispatch(addTomatosDone())
      setDoneTasks(doneTasks + 1)
      dispatch(setTomatoTime(timerTomatoNum))

      if (tasks.length > 0) {
        dispatch(removeTask(0))
      }
    } else {
      dispatch(setStops())
    }

    setTimerTomatoNum(0)
    dispatch(setFocus())
  }

  const stopRestTimer = () => {
    setRest(false)
    setTime(WORK_TIME_SEC)
    setOver(false)
    setStart(false)
    setPause(false)
    dispatch(addTomatosDone())

    if (tasks.length > 0) {
      dispatch(denyTomatosTotal(0))

      if (tasks[0].tomatos === 1) {
        dispatch(removeTask(0))
        setDoneTasks(doneTasks + 1)
        dispatch(setTomatoTime(timerTomatoNum))
      }
    }
  }

  return (
    <div
      className={`
      ${styles.timer}
      ${start ? styles.isStart : ''}
      ${pause ? styles.isPause : ''}
      ${rest ? styles.isRest : ''}`}
    >
      <div className={styles.timerHead}>
        <h3>{tasks.length > 0 ? tasks[0].name : `Задача ${doneTasks + 1}`}</h3>
        <span>
          {rest ? `Перерыв ${restCount + 1}` : `Помидор ${tomato + 1}`}
        </span>
      </div>
      <div className={styles.timerBody}>
        <div className={styles.timerInner}>
          <div className={styles.timerTime}>
            <span>{getTimeSec(time)}</span>
            <button onClick={() => setNewTime()}>
              <PlusIcon />
            </button>
          </div>
          <div className={styles.timerText}>
            {`Задача ${doneTasks + 1} `}
            <span>{tasks.length > 0 ? tasks[0].name : ''}</span>
          </div>
          <div className={styles.timerActions}>
            <Button
              title={setTitleLeftBtn()}
              color="green"
              onClick={() => (start ? pauseTimer() : setStart(true))}
            />
            <Button
              title={setTitleRightBtn()}
              color="red"
              border={!pause}
              disabled={!start && tomato === 0}
              onClick={stopTimer}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Timer
