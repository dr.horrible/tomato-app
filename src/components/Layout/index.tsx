interface ILayoutProps {
  children: React.ReactNode
}

function Layout({children}: ILayoutProps) {
  return <div className='container'>
    {children}
  </div>
}

export default Layout