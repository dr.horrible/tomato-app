import { ReactElement } from 'react'
import './index.scss'

interface IButtonProps {
  color: string
  title: string
  border?: boolean
  disabled?: boolean
  onClick?: () => void
  type?: 'button' | 'submit' | 'reset'
}

function Button({
  color,
  title,
  border,
  onClick,
  disabled = false,
  type = 'button',
}: IButtonProps): ReactElement {
  return (
    <button
      onClick={onClick}
      className={`button ${color}${border ? '_border' : ''}`}
      disabled={disabled}
      type={type}
    >
      {title}
    </button>
  )
}

export default Button
