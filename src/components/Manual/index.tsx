import { v4 as uuidv4 } from 'uuid'
import { MANUAL_DATA } from '../../constants'

function Manual() {
  return (
    <ul>
      {MANUAL_DATA.map((item) => {
        return <li key={uuidv4()}>{item}</li>
      })}
    </ul>
  )
}

export default Manual
