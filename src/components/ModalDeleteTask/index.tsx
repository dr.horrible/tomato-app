import { Dispatch, SetStateAction } from 'react'
import { useAppDispatch } from '../../hooks/useAppDispatch'
import { hideModal } from '../../store/reducer'
import Button from '../Button'
import ModalLayout from '../ModalLayout'
import styles from './index.module.scss'

interface IModalDeleteTasProps {
  handleClick: Dispatch<SetStateAction<boolean>>
}

function ModalDeleteTask({ handleClick }: IModalDeleteTasProps) {
  const dispatch = useAppDispatch()

  return (
    <ModalLayout>
      <div className={styles.modal}>
        <h2>Удалить задачу?</h2>
        <div className={styles.buttons}>
          <Button
            color='red'
            title='Удалить'
            onClick={() => handleClick(true)}
          />
          <div className={styles.cancel}>
            <button onClick={() => dispatch(hideModal)}>Отмена</button>
          </div>
        </div>
      </div>
    </ModalLayout>
  )
}

export default ModalDeleteTask
