import { configureStore } from '@reduxjs/toolkit'
import appReducer from './reducer'
import { loadState } from './localStorage'

const persistedState = loadState()

export const store = configureStore({
  reducer: {
    app: appReducer,
  },
  preloadedState: persistedState,
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
