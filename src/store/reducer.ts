import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { sortDaysOfWeek } from '../helpers/sortDaysOfWeek'

export interface ITasks {
  name: string
  tomatos: number
  id: string
}
interface IInitialState {
  tasks: Array<ITasks>
  isModalOpen: boolean
  statistic: Array<IStatistic>
}

export interface IStatistic {
  date: string
  day: number
  tomato: number
  tomatoTime: number
  workTime: number
  pauseTime: number
  totalTime: number
  focus: number
  stops: number
}

const initialState: IInitialState = {
  tasks: [],
  isModalOpen: false,
  statistic: [],
}

const calcFocusTime = (state: IInitialState) => {
  const focus =
    (state.statistic[0].tomatoTime / state.statistic[0].totalTime) *
    100

  return Math.round(focus)
}

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    createData: (state) => {
      if (
        !state.statistic.find(
          (item) => item.date === new Date().toDateString()
        )
      ) {
        state.statistic.unshift({
          date: new Date().toDateString(),
          day: sortDaysOfWeek(new Date().toDateString()),
          tomato: 0,
          tomatoTime: 0,
          workTime: 0,
          pauseTime: 0,
          totalTime: 0,
          focus: 0,
          stops: 0,
        })
      }
    },
    setWorkTime: (state) => {
      state.statistic[0].workTime += 1
    },
    setPauseTime: (state) => {
      state.statistic[0].pauseTime += 1
    },
    setTomatoTime: (state, action: PayloadAction<number>) => {
      state.statistic[0].tomatoTime += action.payload
    },
    setTotalTime: (state) => {
      state.statistic[0].totalTime += 1
    },
    setFocus: (state) => {
      state.statistic[0].focus = calcFocusTime(state)
    },
    setStops: (state) => {
      state.statistic[0].stops += 1
    },
    showModal: (state) => {
      state.isModalOpen = true
    },
    hideModal: (state) => {
      state.isModalOpen = false
    },
    addTomatosTotal: (state, action: PayloadAction<number>) => {
      state.tasks[action.payload].tomatos += 1
    },
    denyTomatosTotal: (state, action: PayloadAction<number>) => {
      state.tasks[action.payload].tomatos -= 1
    },
    addTomatosDone: (state) => {
      state.statistic[0].tomato += 1
    },
    addTask: (state, action: PayloadAction<ITasks>) => {
      state.tasks.unshift(action.payload)
    },
    removeTask: (state, action: PayloadAction<number>) => {
      const tasksArr = [...state.tasks]

      tasksArr.splice(action.payload, 1)

      state.tasks = tasksArr
    },
    editTaskTitle: (
      state,
      action: PayloadAction<{
        index: number
        prevTitle: string
        newTitle: string
      }>
    ) => {
      const tasksArr = [...state.tasks]

      const newTaskTomatos = tasksArr[action.payload.index].tomatos
      const newTaskId = tasksArr[action.payload.index].id

      tasksArr.splice(action.payload.index, 1, {
        name: action.payload.newTitle,
        tomatos: newTaskTomatos,
        id: newTaskId
      })

      state.tasks = tasksArr
    },
  },
})

export const {
  createData,
  setWorkTime,
  setPauseTime,
  setTomatoTime,
  setTotalTime,
  setFocus,
  setStops,
  showModal,
  hideModal,
  addTomatosTotal,
  addTomatosDone,
  denyTomatosTotal,
  addTask,
  removeTask,
  editTaskTitle,
} = appSlice.actions

export default appSlice.reducer
