import { Route, Routes, useLocation } from 'react-router-dom'
import Header from './components/Header'
import Layout from './components/Layout'
import MainPage from './pages/MainPage'
import StatisticPage from './pages/StatisticPage'
import { STATISTICS_PATH } from './constants'
import { useDispatch } from 'react-redux'
import { createData } from './store/reducer'

function App() {
  const location = useLocation()
  const dispatch = useDispatch()

  dispatch(createData())

  return (
    <div className="app">
      <Header href={location.pathname === '/' ? `/${STATISTICS_PATH}` : '/'} />
      <div className="main">
        <Layout>
          <Routes>
            <Route index element={<MainPage />} />
            <Route path={STATISTICS_PATH} element={<StatisticPage />} />
          </Routes>
        </Layout>
      </div>
    </div>
  )
}

export default App
