import { useEffect, useState } from 'react'
import { ClockIcon } from '../../assets/svg/ClockIcon'
import { FocusIcon } from '../../assets/svg/FocusIcon'
import { HappyTomato } from '../../assets/svg/HappyTomato'
import { SimpleTomato } from '../../assets/svg/SimpleTomato'
import { StopIcon } from '../../assets/svg/StopIcon'
import Chart, { IData } from '../../components/Chart'
import { FilterSelect } from '../../components/FilterSelect'
import { DAY_FULL_NAMES } from '../../constants'
import { createChartData } from '../../helpers/createChartData'
import { getTimeString, getTimeSecondsString } from '../../helpers/getTime'
import { wordForm } from '../../helpers/wordForm'
import { useAppSelector } from '../../hooks/useAppSelector'
import { mockData } from '../../mockData'
import { IStatistic } from '../../store/reducer'
import styles from './index.module.scss'

function StatisticPage() {
  const statistic = useAppSelector((state) => state.app.statistic)
  const [activeDay, setActiveDay] = useState(NaN)
  const [filterVal, setFilterVal] = useState('currentWeek')
  const [chartData, setChartData] = useState(Array<IData>)
  const [dateData, setDateData] = useState<undefined | IStatistic>(undefined)

  useEffect(() => {
    const weekDay = statistic[0].day

    setActiveDay(weekDay)
  }, [statistic])

  useEffect(() => {
    const data = createChartData(statistic, filterVal)

    setChartData(data)
  }, [filterVal])

  useEffect(() => {
    if ((activeDay || activeDay === 0) && chartData.length > 0) {
      const statisticItem = statistic.find(
        (item) => item.date === chartData[activeDay].value
      )

      setDateData(statisticItem)
    }
  }, [activeDay, chartData])

  return (
    <>
      <div className={styles.head}>
        <h1>Ваша активность</h1>
        <div className={styles.select}>
          <FilterSelect onChangeSelect={setFilterVal} />
        </div>
      </div>
      <div className={styles.main}>
        <div className={styles.mainBody}>
          <div className={styles.mainBodyCol}>
            <div className={styles.day}>
              <h2>{DAY_FULL_NAMES[activeDay]}</h2>
              {dateData ? (
                <p>
                  Вы&nbsp;работали над задачами в&nbsp;течение&nbsp;
                  <span>{getTimeString(dateData.totalTime / 60)}</span>
                </p>
              ) : (
                <p>Нет данных</p>
              )}
            </div>
            <div className={styles.tomato}>
              {dateData ? (
                <>
                  <div className={styles.tomatoMain}>
                    <SimpleTomato />
                    <span>x{dateData.tomato}</span>
                  </div>
                  <div className={styles.tomatoBottom}>
                    {dateData.tomato +
                      ' ' +
                      wordForm(dateData.tomato, [
                        'помидор',
                        'помидора',
                        'помидоров',
                      ])}
                  </div>
                </>
              ) : (
                <div className={styles.tomatoMain}>
                  <HappyTomato />
                </div>
              )}
            </div>
          </div>
          <div className={styles.chart}>
            <Chart
              data={chartData}
              onChange={setActiveDay}
              active={activeDay}
            />
          </div>
        </div>
        <div className={styles.mainBottom}>
          <div
            className={`${styles.bottomBlock} ${styles.isOrange} ${
              dateData ? '' : styles.isEmpty
            }`}
          >
            <div className={styles.bottomBlockRow}>
              <div className={styles.bottomBlockInfo}>
                <div className={styles.bottomBlockTitle}>Фокус</div>
                <div className={styles.bottomBlockNum}>
                  {dateData ? dateData.focus : 0}%
                </div>
              </div>
              <FocusIcon />
            </div>
          </div>
          <div
            className={`${styles.bottomBlock} ${styles.isPurple} ${
              dateData ? '' : styles.isEmpty
            }`}
          >
            <div className={styles.bottomBlockRow}>
              <div className={styles.bottomBlockInfo}>
                <div className={styles.bottomBlockTitle}>Время на паузе</div>
                <div className={styles.bottomBlockNum}>
                  {dateData
                    ? getTimeSecondsString(dateData.pauseTime, true)
                    : 0}
                </div>
              </div>
              <ClockIcon />
            </div>
          </div>
          <div
            className={`${styles.bottomBlock} ${styles.isBlue} ${
              dateData ? '' : styles.isEmpty
            }`}
          >
            <div className={styles.bottomBlockRow}>
              <div className={styles.bottomBlockInfo}>
                <div className={styles.bottomBlockTitle}>Остановки</div>
                <div className={styles.bottomBlockNum}>
                  {dateData ? dateData.stops : 0}
                </div>
              </div>
              <StopIcon />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default StatisticPage
