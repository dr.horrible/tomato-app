import Manual from '../../components/Manual'
import TaskForm from '../../components/TaskForm'
import Timer from '../../components/Timer'
import styles from './index.module.scss'

function MainPage() {
  return (
    <>
      <div className={styles.row}>
        <div className={styles.tasks}>
          <h1>Ура! Теперь можно начать работать:</h1>
          <Manual />
          <TaskForm />
        </div>
        <div className={styles.timer}>
          <Timer />
        </div>
      </div>
    </>
  )
}

export default MainPage
