import { wordForm } from './wordForm'

export const getTimeSec = (mins: number) => {
  const minutes = Math.trunc(mins / 60)
  const seconds = mins % 60

  return `${minutes}:${seconds > 9 ? seconds : '0' + seconds}`
}

export const getTimeSecondsString = (sec: number, short: boolean = false) => {
  const minutes = Math.trunc(sec / 60)
  const seconds = sec % 60
  const minutesWord = short
    ? 'мин'
    : wordForm(minutes, ['минута', 'минуты', 'минут'])

  if (minutes < 1) {
    return `${seconds} сек`
  } else {
    return `${minutes + ' ' + minutesWord} ${
      seconds === 0 ? '' : seconds + 'сек'
    }`
  }
}

export const getTimeString = (mins: number, short: boolean = false) => {
  const hours = Math.trunc(mins / 60)
  const minutes = mins % 60
  const hoursWord = short ? 'ч' : wordForm(hours, ['час', 'часа', 'часов'])
  const minutesWord = short
    ? 'мин'
    : wordForm(minutes, ['минута', 'минуты', 'минут'])

  if (hours < 1) {
    return getTimeSecondsString(minutes * 60, short)
  } else {
    return `${hours + ' ' + hoursWord} ${
      minutes === 0 ? '' : minutes + ' ' + minutesWord
    }`
  }
}
