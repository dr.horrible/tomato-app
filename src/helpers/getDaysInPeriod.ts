import { IStatistic } from '../store/reducer'

export const getDaysInPeriod = (arr: Array<IStatistic>, period: string) => {
  const start = new Date()
  const end = new Date()

  let dateQ: number = 0

  switch (period) {
    case 'lastWeek':
      dateQ = 7
      break
    case 'beforeLastWeek':
      dateQ = 14
      break
  }

  start.setDate(new Date().getDate() - dateQ - (new Date().getDay() - 1))
  end.setDate(new Date().getDate() - (dateQ - 1) + (7 - new Date().getDay()))

  const firstDate = new Date(
    start.getFullYear(),
    start.getMonth(),
    start.getDate()
  )
  const lastDate = new Date(end.getFullYear(), end.getMonth(), end.getDate())

  return arr.filter(
    (item) =>
      new Date(item.date) >= firstDate && new Date(item.date) < lastDate && item
  )
}
