export const wordForm = (num: number, words: Array<string>) => {
  num = Math.abs(num) % 100
  const n = num % 10
  if (num > 10 && num < 20) {
    return words[2]
  }
  if (n > 1 && n < 5) {
    return words[1]
  }
  if (n === 1) {
    return words[0]
  }
  return words[2]
}
