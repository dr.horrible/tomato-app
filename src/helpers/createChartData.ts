import { IStatistic } from '../store/reducer'
import { getDaysInPeriod } from './getDaysInPeriod'

export const createChartData = (arr: Array<IStatistic>, period: string) => {
  const filteredDates = []
  const dates = getDaysInPeriod(arr, period)

  for (let i = 0; i < 7; i++) {
    if (dates.find((item) => item.day === i)) {
      const idx = dates.find((item) => item.day === i)

      filteredDates.push({
        value: idx?.date,
        time: idx?.totalTime,
      })
    } else {
      filteredDates.push({
        value: '',
        time: 0,
      })
    }
  }

  return filteredDates
}
