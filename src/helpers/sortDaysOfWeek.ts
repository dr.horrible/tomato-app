export const sortDaysOfWeek = (date: string) => {
  if (new Date(date).getDay() - 1 === -1) {
    return 6
  } else {
    return new Date(date).getDay() - 1
  }
}
